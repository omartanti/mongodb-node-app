//Using ES6 Destructuring method
const {MongoClient, ObjectID} = require('mongodb');

//MongoDb gives you a default database test @ mongodb://localhost:27017/test
//In MongoDb you do not need to create the database before start using it, it will be created automatically
//MongoDb will only create the database when we start adding data to it, not by just connecting to it
MongoClient.connect('mongodb://localhost:27017/TodoApp', {useNewUrlParser: true}, (err, client) => {

    if (err) {
        return console.log('Unable to connect to MongoDb Server');
    }

    //Fetch the database before we can start using it
    const db = client.db('TodoApp');

    //Select from Todos Collection
    //.find() returns a mongodb cursor (pointer)
    //.toArray() returns a promise
    db.collection('Todos').find({
        _id: new ObjectID('5bef9fcc9991bb0cb1051269') //The _id is not just a string but an object of ObjectID
    }).toArray().then((docs) => {
        console.log('Todos:');
        console.log(JSON.stringify(docs, undefined, 2));
    }, (err) => {

    });

    //Use .toCount
    db.collection('Todos').find({completed: false}).count().then((count) => {
        console.log(`Count of Todos to be completed: ${count}`);
    }, (err) => {

    });

    //Search through Users
    db.collection('Users').find({name: 'Omar'}).toArray().then((docs) => {
        console.log('Users:');
        console.log(JSON.stringify(docs, undefined, 2));
    }, (err) => {

    });

    //This closes the connection with the DB Server
    // client.close();

});