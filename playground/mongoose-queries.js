const {ObjectID} = require('mongodb');
const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

const todoID = '5bf8d72c1e38dd2a22108db2';

if (ObjectID.isValid(todoID)) {

    Todo.find({
        _id: todoID
    }).then((todos) => {
        console.log('Todos', todos);
    });

    Todo.findOne({
        _id: todoID
    }).then((todo) => {
        if (!todo) {
            return console.log('Todo with ID not found');
        }
        console.log('Todo', todo);
    });

    Todo.findById(todoID).then((todoById) => {
        if (!todoById) {
            return console.log('Todo with ID not found');
        }
        console.log('Todo', todoById);
    }).catch((e) => {
        console.log(e);
    });

}

const UserId = '5bf64dbebd236780bf60296c';

User.findById(UserId).then((user) => {
    if (!user) {
        return console.log('Unable to find User');
    }
    console.log(JSON.stringify(user, undefined, 2))
}, (e) => {
    console.log('Errors fetching User: ', e.message);
});

