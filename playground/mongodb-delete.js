//Using ES6 Destructuring method
const {MongoClient, ObjectID} = require('mongodb');

//MongoDb gives you a default database test @ mongodb://localhost:27017/test
//In MongoDb you do not need to create the database before start using it, it will be created automatically
//MongoDb will only create the database when we start adding data to it, not by just connecting to it
MongoClient.connect('mongodb://localhost:27017/TodoApp', {useNewUrlParser: true}, (err, client) => {

    if (err) {
        return console.log('Unable to connect to MongoDb Server');
    }

    //Fetch the database before we can start using it
    const db = client.db('TodoApp');

    //deleteMany - targets many documents and deltes them
    db.collection('Todos').deleteMany({text: 'Eat Lunch'}).then((result) => {
        console.log(result);
    }, (err) => {

    });

    //deleteOne - targets only one document
    db.collection('Todos').deleteOne({text: 'Eat Lunch'}).then((result) => {
        console.log(result);
    }, (err) => {

    });

    //findOneAndDelete - returns the document we are going to delete
    db.collection('Todos').findOneAndDelete({completed: false}).then((result) => {
        console.log(JSON.stringify(result, undefined, 2));
    }, (err) => {

    });

    //Delete Many from Users collection
    db.collection('Users').deleteMany({name: 'Omar'});

    //Find one and delete from users
    db.collection('Users').findOneAndDelete({
        _id: new ObjectID('5bf23ed8b478953bb681a147')
    }).then((result) => {
        console.log(JSON.stringify(result, undefined, 2));
    });

    //This closes the connection with the DB Server
    // client.close();

});