//Using ES6 Destructuring method
const {MongoClient, ObjectID} = require('mongodb');

//MongoDb gives you a default database test @ mongodb://localhost:27017/test
//In MongoDb you do not need to create the database before start using it, it will be created automatically
//MongoDb will only create the database when we start adding data to it, not by just connecting to it
MongoClient.connect('mongodb://localhost:27017/TodoApp', {useNewUrlParser: true}, (err, client) => {

    if (err) {
        return console.log('Unable to connect to MongoDb Server');
    }

    //Fetch the database before we can start using it
    const db = client.db('TodoApp');

    //findOneAndUpdate - From Todos
    db.collection('Todos').findOneAndUpdate({
        _id: new ObjectID('5bf24175b478953bb681a23d')
    }, {
        $set: {
            completed: true
        }
    }, {
        returnOriginal: false //to return the updated doc
    }).then((result) => {
        console.log(result);
    });

    //findOneAndUpdate - From Users with $set and $inc
    //Reference:
    //https://docs.mongodb.com/manual/reference/method/db.collection.findOneAndUpdate/#db.collection.findOneAndUpdate
    db.collection('Users').findOneAndUpdate({
       _id: new ObjectID("5befa86950a8a00e96656398")
    }, {
        $set: {name: 'Omar'}, //Updates the name to 'Omar'
        $inc: { age: 1} //Increments the age by 1
    }, {
        returnOriginal: false
    }).then((result) => {
        console.log(result);
    });

    //This closes the connection with the DB Server
    // client.close();

});