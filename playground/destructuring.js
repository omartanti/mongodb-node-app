
let user = {name: 'Omar', age: 31, location: 'Fgura'};

//ES6 destructuring method
let {name} = user; //This will create a variable `name` and store the value in user.name
console.log(name); //This will print omar

//This will create 2 variables `age` and `location` and store the values user.age and user.location respectively
let {age, location} = user;
console.log(age); //This will print 31
console.log(location); //This will print 'Fgura'