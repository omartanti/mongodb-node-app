const {ObjectID} = require('mongodb');
const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

// Todo.deleteMany({}).then((result) => {
//     console.log(result);
// });

Todo.findOneAndDelete({_id: '5c03cab68d99e4373d55da42'});

Todo.findByIdAndDelete('5c03cab68d99e4373d55da42').then((todo) => {
    console.log(todo);
});

