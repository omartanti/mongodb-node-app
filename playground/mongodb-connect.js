// const MongoClient = require('mongodb').MongoClient;

//Using ES6 Destructuring method
const {MongoClient, ObjectID} = require('mongodb');

let obj = new ObjectID();
console.log(obj); //This will print a new unique identifier (same format used by mongodb in _id property)

//MongoDb gives you a default database test @ mongodb://localhost:27017/test
//In MongoDb you do not need to create the database before start using it, it will be created automatically
//MongoDb will only create the database when we start adding data to it, not by just connecting to it
MongoClient.connect('mongodb://localhost:27017/TodoApp', {useNewUrlParser: true}, (err, client) => {

    if (err) {
        return console.log('Unable to connect to MongoDb Server');
    }
    console.log('Connected to MongoDb Server');

    //Fetch the database before we can start using it
    const db = client.db('TodoApp');

    //A collections is similar to a database in MySql
    // db.collection('Todos').insertOne({
    //     text: "Something to do",
    //     completed: false,
    // }, (err, result) => {
    //     if (err) {
    //         return console.log('Unable to insert todo', err);
    //     }
    //     console.log(JSON.stringify(result.ops, undefined, 2));
    // });

    //Insert new doc in users collection
    db.collection('Users').insertOne({
        name: 'Donatella',
        age: 31,
        location: 'Fgura'
    }, (err, result) => {
        if (err) {
            return console.log('Unable to insert user', err);
        }

        //We can print the result like the following
        console.log(JSON.stringify(result.ops, undefined, 2));

        //We can also fetch the timestamp from the id
        console.log('Extracting timestamp from _id property: ', result.ops[0]._id.getTimestamp());
    });

    client.close(); //This closes the connection with the DB Server

});