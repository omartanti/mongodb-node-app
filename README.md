**Installing MongoDb Server**

    1.  Install MongoDb on machine from https://www.mongodb.com/
    2.  Extract the dowload
    3.  Rename folder to mongo
    4.  Move mongo folder to ~/
    5.  Create a 'mongo-data' next to the 'mongo' folder
    6.  In terminal run 'cd ~/mongo/bin'
    7.  To start the mongodb server run the following cmd in terminal './mongod --dbpath ~/mongo-data/'
    8.  Points 6 and 7 can be combined in one command: '~/mongo/bin/mongod --dbpath ~/mongo-data/'
     
     
**Using MongoDb in our app**

    1. Install mongodb through npm : "npm i mongodb --save"
    2. Require mongodb module in javascript file: "const MongoClient = require('mongodb').MongoClient;"
    3. Connect to the Database: "MongoClient.connect('mongodb://localhost:27017/TodoApp', {useNewUrlParser: true}, (err, client) => {});"
    4. Inside the connect callback we need to fetch the database before using it: "const db = client.db('TodoApp');"
    
**Unique Identifier (_id) information**

    1. The unique identifier that is generated automatically is made up of:
        a. Timestamp
        b. Machine Identifier
        c. Process Identifier
        d. An incremental count
    2. We can easily extract the timestamp from the '_id' property by using: getTimestamp()
    
**Connecting to Heroku MongoDB**

    1. By running `heroku config` in terminal we can see the MONGO_DB env variable
    2. For e.g. mongodb://heroku_92r20p5r:e4trra9po1t3dvxatfp4q3jzzz@ds121814.mlab.com:21814/heroku_92r20p5p
        a. Where the string before the ':' is the user name i.e. heroku_92r20p5r
        b. The string between ':' and '@' is the password: i.e. e4trra9po1t3dvxatfp4q3jzzz
        c. The string after the '@' is the endpoint i.e. ds121814.mlab.com:21814
        d. The database name is the last part of the string i.e. heroku_92r20p5p
    3. With these credentials we can create an Authentication Connection in Robo 3T
    
**Advanced Postman Options**

    1. Since we are now setting an x-auth header with nearly every request we have to go through all the requests in postman and update its value manually
    2. To avoid this manual procedure we can make use of 'Tests' tab when registering/logging in in order to set an ENV Variable in postman
    3. Then we can set all the 'x-auth' headers pointing to this environment variable, therefore all requests will make use of the new token automatically
    4. To set the ENV Variable go in the 'Tests' tab for both Register and Login Routes and insert the following code:
```javascript
var token = postman.getResponseHeader('x-auth');
postman.setEnvironmentVariable('node-todo-x-auth', token);
```
    5. Now when you send a request to the Register/Login routes this ENV Variable will be set
    6. In order to use it in the other requests headers you only need to set the key as 'x-auth' and its value to '{{node-todo-x-auth}}'
   