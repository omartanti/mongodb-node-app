let mongoose = require('mongoose');

mongoose.Promise = global.Promise;

//`heroku addons:create mongolab:sandbox` command generates the MONGODB_URI env variable
mongoose.connect(
    process.env.MONGODB_URI,
    {useNewUrlParser: true}
);

module.exports = {mongoose};