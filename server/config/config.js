let env = process.env.NODE_ENV || 'development';

if(env === 'development' || env === 'test'){

    //When calling require for a json file it parses it to an object automatically
    let config = require('./config.json');
    let envConfig = config[env];
    Object.keys(envConfig).forEach((key) => {
        process.env[key] = envConfig[key];
    });

}