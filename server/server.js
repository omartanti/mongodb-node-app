require('./config/config');

const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const {ObjectID} = require('mongodb');

let {mongoose} = require('./db/mongoose');
let {Todo} = require('./models/todo');
let {User} = require('./models/user');
let {authenticate} = require('./middleware/authenticate');

let app = express();
const port = process.env.PORT;

app.use(bodyParser.json());

app.post('/todos', authenticate, (req, res) => {

    let todo = new Todo({
        text: req.body.text,
        _creator: req.user._id
    });

    todo.save().then((doc) => {
        res.send(doc);
    }, (e) => {
        res.status(400).send(e);
    });

});

app.get('/todos', authenticate, (req, res) => {
    Todo.find({
        _creator: req.user._id
    }).then((todos) => {
        res.send({todos});
    }, (e) => {
        res.status(400).send(e);
    })
});

app.get('/todos/:id', authenticate, (req, res) => {
    let id = req.params.id;

    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Todo.findOne({
        _id: id,
        _creator: req.user._id
    }).then((todo) => {
        if (!todo) {
            return res.status(404).send();
        }
        res.send({todo});
    }, (e) => {
        res.status(400).send();
    });
});

app.delete('/todos/:id', authenticate, async (req, res) => {

    const id = req.params.id;
    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    try{
        const todo = await Todo.findOneAndDelete({
            _id: id,
            _creator: req.user._id
        });
        if (!todo) {
            return res.status(404).send();
        }
        res.send({todo});
    }catch(e){
        res.status(400).send();
    }

});

app.patch('/todos/:id', authenticate, async (req, res) => {
    const id = req.params.id;

    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    //this only picks certain elements (2nd param) from the object (1st param) if they exist
    let body = _.pick(req.body, ['text', 'completed']);

    //Check the completed flag and set the completedAt
    if (_.isBoolean(body.completed) && body.completed) {
        body.completedAt = new Date().getTime();
    } else {
        body.completedAt = null;
        body.completed = false;
    }

    try{
        //Update the Record
        let todo = await Todo.findOneAndUpdate({
            _id: id,
            _creator: req.user._id
        }, {$set: body}, {new: true});
        if (!todo) {
            return res.status(404).send();
        }
        return res.send({todo});
    }catch(e){
        return res.status(400).send();
    }

});

//POST /users - User Registration
app.post('/users', async (req, res) => {

    try{
        const body = _.pick(req.body, ['email', 'password']);
        let user = new User(body);
        user = await user.save();
        //Since the custom function is added with '.methods' the function will be used as instance method i.e. user.generateAuthToken
        //Therefore we bind the function to the user document
        const token = await user.generateAuthToken();
        //We need to add the token in the response header
        //Headers starting with 'x-' means they are custom and used for specific purposes
        res.header('x-auth', token).send(user);
    }catch(e){
        res.status(400).send(e);
    }

});


// GET /users/me - This route will find the associated user and sending the user back
//We are also using the 'authenticate' middleware with this route
app.get('/users/me', authenticate, (req, res) => {
    res.send(req.user);
});

//POST /users/login - User Login with email and password
app.post('/users/login', async (req, res) => {

    try{
        const body = _.pick(req.body, ['email', 'password']);
        const user = await User.findByCredentials(body.email, body.password);
        const token = await user.generateAuthToken();
        res.header('x-auth', token).send(user);
    }catch(e){
        res.status(400).send();
    }

});

app.delete('/users/me/token', authenticate, async (req, res) => {
    try{
        await req.user.removeToken(req.token);
        res.status(200).send();
    }catch(e){
        res.status(400).send();
    }
});

if (!module.parent) {
    app.listen(port, () => {
        console.log(`Started on port ${port}`);
    });
}

module.exports = {app};