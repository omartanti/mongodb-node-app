const {ObjectID} = require('mongodb');
const jwt = require('jsonwebtoken');
const {Todo} = require('../../models/todo');
const {User} = require('../../models/user');

const userOneID = new ObjectID();
const userTwoID = new ObjectID();

const users = [{
    _id: userOneID,
    email: 'omar@gmail.com',
    password: 'userOnePass',
    tokens: [{
        access: 'auth',
        token: jwt.sign({_id: userOneID, access: 'auth'}, process.env.JWT_SECRET).toString()
    }]
}, {
    _id: userTwoID,
    email: 'jen@example.com',
    password: 'userTwoPass',
    tokens: [{
        access: 'auth',
        token: jwt.sign({_id: userTwoID, access: 'auth'}, process.env.JWT_SECRET ).toString()
    }]
}];

const todos = [
    {
        _id: new ObjectID(),
        text: 'Eat Breakfast Test',
        _creator: userOneID
    },
    {
        _id: new ObjectID(),
        text: 'Clean Dishes Test',
        completed: true,
        completedAt: 333,
        _creator: userTwoID
    },
];

const clearTodos = (done) => {
    Todo.deleteMany({}).then(() => done());
};

const populateTodos = (done) => {
    Todo.deleteMany({}).then(() => {
        return Todo.insertMany(todos);
    }).then(() => done());
};

const populateUsers = (done) => {
    User.deleteMany({}).then(() => {

        //We need to use '.save()' method instead of 'insertMany' to make sure that the middleware runs and passwords are hashed
        let userOne = new User(users[0]).save();
        let userTwo = new User(users[1]).save();

        //The 'Promise.all' make sure that all promises are executed before performing next functionality
        return Promise.all([userOne, userTwo]);

    }).then(() => done());
};

module.exports = {todos, populateTodos, clearTodos, users, populateUsers};