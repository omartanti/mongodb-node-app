const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

let UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
        unique: true,
        validate: {
            // validator: validator.isEmail,  {This is ES6 shorthand for the below validator function}
            validator: (value) => {
                return validator.isEmail(value);
            },
            message: '{VALUE} is not a valid email'
        }
    },
    password: {
        type: String,
        require: true,
        minlength: 6
    },
    tokens: [{
        access: {
            type: String,
            require: true
        },
        token: {
            type: String,
            require: true
        }
    }]
});

//Here we are going to modify an existing method in the schema
//This will make sure that we only return the necessary data only when sending our response to user
UserSchema.methods.toJSON = function () {
    let user = this; // this is equal to the user document/record

    //This will convert the mongoose object to a regular object
    let userObject = user.toObject();

    //We will be needing to return only the _id and email property therefore eliminating tokens and password
    return _.pick(userObject, ['_id', 'email']);
};

//Here we create a custom method to the schema in order to generate auth token and assign them to a user
//Since we need the this keyword we will be using the old function() syntax
UserSchema.methods.generateAuthToken = function () {
    let user = this; // this is equal to the user document/record
    let access = 'auth';
    let token = jwt.sign({_id: user._id.toHexString(), access}, process.env.JWT_SECRET).toString();

    user.tokens = user.tokens.concat([{access, token}]);

    //We will return the promise so who calls the generateAuthToken method can use .then() to it
    return user.save().then(() => {
        //We also return the token to be used in the .then() from where this method is being called
        return token;
    });
};

UserSchema.methods.removeToken = function (token) {
    let user = this;

    //$pull is a mongoose functionality to remove any matching particular element from an object
    return user.update({
        $pull: {
            tokens: {token}
        }
    });
};

//When adding function with '.statics' the function will be used as model method i.e. User.findByToken
//Whilst when adding with '.methods' the function will be used as instance method i.e. user.generateAuthToken
UserSchema.statics.findByToken = function (token) {
    let User = this; // this is equal to the User Model
    let decoded;

    try {
        decoded = jwt.verify(token, process.env.JWT_SECRET);
    } catch (e) {
        return Promise.reject();
        //The above is a shorthand for:
        // return new Promise((resolve, reject) => {
        //     reject();
        // });
    }

    //We need to fetch users where in tokens have a particular token (nested document query)
    //We will also return the promise so when calling findByToken we can chain other .then()
    return User.findOne({
        '_id': decoded._id,
        'tokens.token': token,
        'tokens.access': 'auth'
    });
};

//Create middleware for UserSchema to hash password before saving document to database
UserSchema.pre('save', function (next) {
    var user = this;

    //We need to hash the password only when it is modified else we will be hashing the hashed password infinitely on every user update
    if (user.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
                user.password = hash;
                //This is required in every callback for Mongoose Middleware else the app will stop
                next();
            });
        });
    } else {
        //This is required in every callback for Mongoose Middleware else the app will stop
        next();
    }
});

UserSchema.statics.findByCredentials = function (email, password) {
    let User = this; // this is equal to the User Model

    //We need to make sure that a user is registered with provided email
    return User.findOne({email}).then((user) => {

        //If user not found by email we can reject
        if (!user) {
            return Promise.reject();
        }

        //We have to check the password
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err, res) => {
                if (res) {
                    resolve(user);
                }
                reject();
            });
        });
    });
};

//Create the User Model
let User = mongoose.model('User', UserSchema);

module.exports = {User};