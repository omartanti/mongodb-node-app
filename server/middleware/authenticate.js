let {User} = require('./../models/user');

let authenticate = (req, res, next) => {
    var token = req.header('x-auth');

    //custom method in Model to fetch user by token - found in user.js
    //When adding function with '.statics' the function will be used as model method i.e. User.findByToken
    //Therefore we bind the function to the User model
    User.findByToken(token).then((user) => {
        if (!user) {
            return Promise.reject(); //This will skip to the catch below too.
        }

        req.user = user;
        req.token = token;
        next(); //Call next to continue with functionality
    }).catch((e) => {
        //The catch will be triggered when the try catch fails in findByToken since the Promise returns reject();
        res.status(401).send();
    });
};

module.exports = {authenticate};